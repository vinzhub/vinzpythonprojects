#pyodbc test
import pyodbc

SERVER = 'GAP01-PC\SQLEXPRESS'
DB = 'py_names'

connection = pyodbc.connect("Driver={SQL Server};"
                            "Server=GAP01-PC\SQLEXPRESS;"
                            "Database=py_names;"
                            "Trusted_Connection=yes;")

#use a storedprocedure to insert_new_data
def usesp():
    cursor = connection.cursor()
    cursor.execute("{call stpAddNames3('vinz123', 'eeewe')}")
    connection.commit()

def add():
    cursor = connection.cursor()

    num_user = 10
    for i in xrange(11,30):
        query = '''
            INSERT INTO [py_names].[dbo].[py_user]
            ([user], [contact])
            VALUES
            ('user{0}', '3210{1}')'''.format(i + 1, i)
        cursor.execute(query)
        
    connection.commit()

def select():
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM py_names.dbo.py_user')

    for row in cursor.fetchall():
        print (row)

def delete():
    cursor = connection.cursor()
    query =  '''
            DELETE FROM [py_names].[dbo].[py_user]
            WHERE contact='2223';
            '''
    connection.commit()

def insert():
    cursor = connection.cursor()
    cursor.execute('''
                INSERT INTO [py_names].[dbo].[py_user]
                ([user],[contact])
                VALUES
                ('qweqwe2','2223')
                ''')
    connection.commit()


def __main__():
    #select()
    #insert()
    #add()
    usesp()

if __name__ == '__main__':
    __main__()
