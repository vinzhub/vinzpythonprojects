# pyodbc test no. 2
# -------------------------------------------------------
# -------------------------------------------------------
import pyodbc
import random as rand
# -------------------------------------------------------
# -------------------------------------------------------

# DATABASE CONFIGS
# -------------------------------------------------------
# -------------------------------------------------------
connection = pyodbc.connect("Driver={SQL Server};"
                            "Server=GAP01-PC\SQLEXPRESS;"
                            "Database=ACCOUNTS_DB;"
                            "Trusted_Connection=yes;")
# -------------------------------------------------------
# -------------------------------------------------------

# Random Variables
# -------------------------------------------------------
# -------------------------------------------------------
CITIES = ['Tacloban',
          'Cebu',
          'Manila',
          'Ormoc']

JOBS = ['Student',
        'Teacher',
        'Security Guard',
        'Principal']

FNAMES = ['Lowri',
          'Michelle ',
          'Julie',
          'India',
          'Theresa',
          'Kimberly',
          'Ashley',
          'Darcie',
          'Aidan',
          'Ruby']

SNAMES = ['Owens',
          'Whitehouse',
          'Hammond',
          'Solis',
          'Rodgers',
          'Molina',
          'Mendez',
          'Lloyd',
          'Murray',
          'Lawrence']
# -------------------------------------------------------
# -------------------------------------------------------

def __randomNames__():
    s_name = SNAMES[rand.randrange(len(SNAMES))]
    f_name = FNAMES[rand.randrange(len(FNAMES))]
    return '{0} {1}'.format(f_name, s_name)


def __randomJob__():
    job = JOBS[rand.randrange(len(JOBS))]
    return '{0}'.format(job)


def __randomCity__():
    city = CITIES[rand.randrange(len(CITIES))]
    return '{0}'.format(city)


def __addNewAccounts__():
    cursor = connection.cursor()
    for i in range(100):
        cursor.execute("{call AddNewAccount('" + __randomNames__() + "'" + ", 'q', 'w', '09123456789')}")
    connection.commit()
    print 'Database query successful'


def __main__():
    __addNewAccounts__()
    print 'New accounts successfully added.'


if __name__ == '__main__':
    __main__()
